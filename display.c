#include "definitions.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>


/** EXTERNAL VARIABLES **/

extern GLdouble _window_ratio;
extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;

extern object3d *_first_object;
extern object3d *_selected_object;

extern camera_list * camara1;

extern int pers_paral;
extern int flat_smooth;

// Parámetros de luz 

float sun_dir[] = { 1.0, 1.0, 1.0, 0.0 }; //El 0 indica que es una dirección

float bulb_pos[] = {1.0, 0.0, 2.0, 1.0}; //El 1 indica que es un punto

float cam_angle = 25.0;

float focus_angle = 30.0;

/**
 * @brief Function to draw the axes
 */
void draw_axes()
{
    /*Draw X axis*/
    glColor3f(KG_COL_X_AXIS_R,KG_COL_X_AXIS_G,KG_COL_X_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(KG_MAX_DOUBLE,0,0);
    glVertex3d(-1*KG_MAX_DOUBLE,0,0);
    glEnd();
    /*Draw Y axis*/
    glColor3f(KG_COL_Y_AXIS_R,KG_COL_Y_AXIS_G,KG_COL_Y_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(0,KG_MAX_DOUBLE,0);
    glVertex3d(0,-1*KG_MAX_DOUBLE,0);
    glEnd();
    /*Draw Z axis*/
    glColor3f(KG_COL_Z_AXIS_R,KG_COL_Z_AXIS_G,KG_COL_Z_AXIS_B);
    glBegin(GL_LINES);
    glVertex3d(0,0,KG_MAX_DOUBLE);
    glVertex3d(0,0,-1*KG_MAX_DOUBLE);
    glEnd();
}


/**
 * @brief Callback reshape function. We just store the new proportions of the window
 * @param width New width of the window
 * @param height New height of the window
 */
void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    /*  Take care, the width and height are integer numbers, but the ratio is a GLdouble so, in order to avoid
     *  rounding the ratio to integer values we need to cast width and height before computing the ratio */
    _window_ratio = (GLdouble) width / (GLdouble) height;
}


/**
 * @brief Callback display function
 */
void display(void) {
    glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);

    GLint v_index, v, f;
    object3d *aux_obj = _first_object;

    /* Clear the screen */
    glClear(GL_COLOR_BUFFER_BIT);
    glEnable(GL_CULL_FACE);

    /* Define the projection */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    if (pers_paral == 0){ //  Proyección: Paralelo, ortho, vista cavallieri
        /*When the window is wider than our original projection plane we extend the plane in the X axis*/
        if ((_ortho_x_max - _ortho_x_min) / (_ortho_y_max - _ortho_y_min) < _window_ratio) {
            /* New width */
            GLdouble wd = (_ortho_y_max - _ortho_y_min) * _window_ratio;
            /* Midpoint in the X axis */
            GLdouble midpt = (_ortho_x_min + _ortho_x_max) / 2;
            /*Definition of the projection*/
            glOrtho(midpt - (wd / 2), midpt + (wd / 2), _ortho_y_min, _ortho_y_max, _ortho_z_min, _ortho_z_max);
        } else {/* In the opposite situation we extend the Y axis */
            /* New height */
            GLdouble he = (_ortho_x_max - _ortho_x_min) / _window_ratio;
            /* Midpoint in the Y axis */
            GLdouble midpt = (_ortho_y_min + _ortho_y_max) / 2;
            /*Definition of the projection*/
            //glOrtho(_ortho_x_min, _ortho_x_max, midpt - (he / 2), midpt + (he / 2), _ortho_z_min, _ortho_z_max);
            glOrtho(camara1 -> ortho_x_min, camara1 -> ortho_x_max,camara1 -> ortho_y_min,camara1 -> ortho_y_max,camara1 -> near,camara1 -> far);
        }
    } else {        
    //  Proyección: Perspectiva, frustrum, vista cónica
        glFrustum(  camara1 -> left,    camara1 -> right, 
                    camara1 -> bottom,  camara1 -> top, 
                    camara1 -> near,    camara1 -> far);
    }

    /*  Now we start drawing the object */
    
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(camara1 -> matrix -> matriz_16);
    /*  First, we draw the axes */
    draw_axes();


    //Parametros de luz

    float light_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
    float light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    float light_specular[]= { 1.0, 1.0, 1.0, 1.0 };

    //Sol
    glLightfv(GL_LIGHT0, GL_POSITION, sun_dir);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);

    //Bombilla
    glLightfv(GL_LIGHT1, GL_POSITION, bulb_pos);
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
    glLightf (GL_LIGHT1, GL_SPOT_CUTOFF, 180.0); 

    //Foco cámara
    float cam_pos[] = {camara1->pos.x, camara1->pos.y, camara1->pos.z};
    float cam_dir[] = {camara1->matrix->matriz_16[2], camara1->matrix->matriz_16[6], camara1->matrix->matriz_16[10]};
    glLightfv(GL_LIGHT2, GL_POSITION, cam_pos);
    glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);
    glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, cam_angle);
    glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, cam_dir);



    /*  Now each of the objects in the camera_list  */

    while (aux_obj != 0) {

        //Creamos los vectores para la luz del foco relacionado al objeto

        float focus_pos[] = {   _selected_object->first->matriz_16[12],
                                _selected_object->first->matriz_16[13],
                                _selected_object->first->matriz_16[14],
                                1.0                                    };

        float focus_dir[] = {   _selected_object->first->matriz_16[8],
                                _selected_object->first->matriz_16[9],
                                _selected_object->first->matriz_16[10]};


        //Foco relacionado al objeto
        glLightfv(GL_LIGHT3, GL_POSITION, focus_pos);
        glLightfv(GL_LIGHT3, GL_AMBIENT, light_ambient);
        glLightfv(GL_LIGHT3, GL_DIFFUSE, light_diffuse);
        glLightfv(GL_LIGHT3, GL_SPECULAR, light_specular);
        glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, focus_dir);
        glLightf (GL_LIGHT3, GL_SPOT_CUTOFF, focus_angle);

                      
        /*  Select the color, depending on whether 
            the current object is the selected one or not 
        */
        glPushMatrix(); //Pusheamos el objeto para que no se relacione con el resto
        if (aux_obj == _selected_object){
            
            glColor3f(  KG_COL_SELECTED_R,
                        KG_COL_SELECTED_G,
                        KG_COL_SELECTED_B);
        } else {
            
            glColor3f(  KG_COL_NONSELECTED_R,
                        KG_COL_NONSELECTED_G,
                        KG_COL_NONSELECTED_B);
        }

        /*  Draw the object; for each face create a new polygon 
            with the corresponding vertices 
        */
  
        glMultMatrixf(aux_obj->first->matriz_16);
  
        for (f = 0; f < aux_obj->num_faces; f++) {

            glMaterialfv(GL_FRONT, GL_SPECULAR, aux_obj->mat_specular);
            glMaterialfv(GL_FRONT, GL_AMBIENT, aux_obj->mat_ambient);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, aux_obj->mat_diffuse);
            glMaterialfv(GL_FRONT, GL_SHININESS, aux_obj->mat_shininess);

            glBegin(GL_POLYGON);
            
            for (v = 0; v < aux_obj->face_table[f].num_vertices; v++) {
                
                v_index = aux_obj->face_table[f].vertex_table[v];
                

                glNormal3d (aux_obj->vertex_table[aux_obj->face_table[f].vertex_table[v]].vertNorm.x,
                            aux_obj->vertex_table[aux_obj->face_table[f].vertex_table[v]].vertNorm.y,
                            aux_obj->vertex_table[aux_obj->face_table[f].vertex_table[v]].vertNorm.z);


                glVertex3d (aux_obj->vertex_table[v_index].coord.x,
                            aux_obj->vertex_table[v_index].coord.y,
                            aux_obj->vertex_table[v_index].coord.z);

               }
            glEnd();
        }
        glPopMatrix(); // Le hacemos pop de forma en la que el resto no lo dibuje
        aux_obj = aux_obj->next;
    }
    glLoadIdentity();
    /*Do the actual drawing*/
    glFlush();
    glutSwapBuffers();
}
