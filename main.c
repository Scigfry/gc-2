#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "display.h"
#include "io.h"
#include "definitions.h"


/*
        COMPILAR:

        gcc -o test *.c -lGL -lGLU -lglut -lm

*/

/*
    PIPELINE:

        Modelview -> Projection Matrix -> ...

    Modelview:
*/

/** GLOBAL VARIABLES **/

GLdouble _window_ratio;                     /*Control of window's proportions */
GLdouble _ortho_x_min,_ortho_x_max;         /*Variables for the control of the orthographic projection*/
GLdouble _ortho_y_min ,_ortho_y_max;        /*Variables for the control of the orthographic projection*/
GLdouble _ortho_z_min,_ortho_z_max;         /*Variables for the control of the orthographic projection*/

object3d * _first_object= 0;                /*List of objects*/
object3d * _selected_object = 0;            /*Object currently selected*/

camera_list * camara1 = 0;                  // Las 4 cámaras
camera_list * camara2 = 0;                  // de las que
camera_list * camara3 = 0;                  // haremos uso
camera_list * camara4 = 0;                  // 

extern int flat_smooth;

/** GENERAL INITIALIZATION **/
void initialization (){

    /*Initialization of all the variables with the default values*/
    _ortho_x_min = KG_ORTHO_X_MIN_INIT;
    _ortho_x_max = KG_ORTHO_X_MAX_INIT;
    _ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    _ortho_y_max = KG_ORTHO_Y_MAX_INIT;
    _ortho_z_min = KG_ORTHO_Z_MIN_INIT;
    _ortho_z_max = KG_ORTHO_Z_MAX_INIT;

    _window_ratio = (GLdouble) KG_WINDOW_WIDTH / (GLdouble) KG_WINDOW_HEIGHT;

    /****************************************/

    // Inicializamos las cámaras

    camara1 =           malloc(sizeof(camera_list));
    camara1 -> matrix = malloc(sizeof(matrix_list));
    camara2 =           malloc(sizeof(camera_list));
    camara2 -> matrix = malloc(sizeof(matrix_list));
    camara3 =           malloc(sizeof(camera_list));
    camara3 -> matrix = malloc(sizeof(matrix_list));
    camara4 =           malloc(sizeof(camera_list));
    camara4 -> matrix = malloc(sizeof(matrix_list));

    // Cámara 1

    camara1 -> id = 1;
    camara1 -> matrix -> next = 0;
    
    glMatrixMode(GL_MODELVIEW);
    
    camara1->pos.x = 0.0;
    camara1->pos.y = 0.0;
    camara1->pos.z = 0.0;
    
    glLoadIdentity();
    glGetFloatv(GL_MODELVIEW_MATRIX, camara1->matrix->matriz_16);
        
    camara1 -> left =   -0.1f;
    camara1 -> right =   0.1f;
    camara1 -> bottom = -0.1f;
    camara1 -> top =     0.1f;
    camara1 -> near=     0.1f;
    camara1 -> far =     1000.0f;

    camara1 -> prev =   camara4;
    camara1 -> next =   camara2;

    camara1 -> ortho_x_min = KG_ORTHO_X_MIN_INIT;
    camara1 -> ortho_x_max = KG_ORTHO_X_MAX_INIT;
    camara1 -> ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    camara1 -> ortho_y_max = KG_ORTHO_Y_MAX_INIT;

    // Cámara 2

    camara2 -> id = 2;
    camara2 -> matrix -> next = 0;

    camara2->pos.x = 10.0;
    camara2->pos.y = 15.0;
    camara2->pos.z = 20.0;  

    glMatrixMode(GL_MODELVIEW);

    gluLookAt(10.0, 15.0, 20.0,
              4.0,  4.0,  4.0,
              0.0,  1.0,  1.0 );

    glGetFloatv(GL_MODELVIEW_MATRIX, camara2->matrix->matriz_16);
    glLoadIdentity();

    camara2 -> left =   -0.1f;
    camara2 -> right =   0.1f;
    camara2 -> bottom = -0.1f;
    camara2 -> top =     0.1f;
    camara2 -> near=     0.1f;
    camara2 -> far =     1000.0f;

    camara2 -> prev =   camara1;
    camara2 -> next =   camara3;

    camara2 -> ortho_x_min = KG_ORTHO_X_MIN_INIT;
    camara2 -> ortho_x_max = KG_ORTHO_X_MAX_INIT;
    camara2 -> ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    camara2 -> ortho_y_max = KG_ORTHO_Y_MAX_INIT;
/*
    printf("%f\t%f\t%f\t%f\n", camara2->matrix->matriz_16[0], camara2->matrix->matriz_16[4], camara2->matrix->matriz_16[8],  camara2->matrix->matriz_16[12]);
    printf("%f\t%f\t%f\t%f\n", camara2->matrix->matriz_16[1], camara2->matrix->matriz_16[5], camara2->matrix->matriz_16[8],  camara2->matrix->matriz_16[13]);
    printf("%f\t%f\t%f\t%f\n", camara2->matrix->matriz_16[2], camara2->matrix->matriz_16[6], camara2->matrix->matriz_16[10], camara2->matrix->matriz_16[14]);
    printf("%f\t%f\t%f\t%f\n", camara2->matrix->matriz_16[3], camara2->matrix->matriz_16[7], camara2->matrix->matriz_16[11], camara2->matrix->matriz_16[15]);
*/
    // Cámara 3

    camara3 -> id = 3;
    camara3 -> matrix -> next = 0;

    camara3->pos.x = 5.0;
    camara3->pos.y = 5.0;
    camara3->pos.z = 5.0;  

    glMatrixMode(GL_MODELVIEW);

    gluLookAt(5.0, 5.0, 5.0,
              7.0, 2.5, 5.0,
              0.0, 0.0, 1.0);

    glGetFloatv(GL_MODELVIEW_MATRIX, camara3->matrix->matriz_16);
    glLoadIdentity();

    camara3 -> left =   -0.1f;
    camara3 -> right =   0.1f;
    camara3 -> bottom = -0.1f;
    camara3 -> top =     0.1f;
    camara3 -> near=     0.1f;
    camara3 -> far =     1000.0f;

    camara3 -> prev =   camara2;
    camara3 -> next =   camara4;

    camara3 -> ortho_x_min = KG_ORTHO_X_MIN_INIT;
    camara3 -> ortho_x_max = KG_ORTHO_X_MAX_INIT;
    camara3 -> ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    camara3 -> ortho_y_max = KG_ORTHO_Y_MAX_INIT;

/*
    printf("%f\t%f\t%f\t%f\n", camara3->matrix->matriz_16[0], camara3->matrix->matriz_16[4], camara3->matrix->matriz_16[8],  camara3->matrix->matriz_16[12]);
    printf("%f\t%f\t%f\t%f\n", camara3->matrix->matriz_16[1], camara3->matrix->matriz_16[5], camara3->matrix->matriz_16[8],  camara3->matrix->matriz_16[13]);
    printf("%f\t%f\t%f\t%f\n", camara3->matrix->matriz_16[2], camara3->matrix->matriz_16[6], camara3->matrix->matriz_16[10], camara3->matrix->matriz_16[14]);
    printf("%f\t%f\t%f\t%f\n", camara3->matrix->matriz_16[3], camara3->matrix->matriz_16[7], camara3->matrix->matriz_16[11], camara3->matrix->matriz_16[15]);
*/

    // Cámara 4

    camara4 -> id = 4;
    camara4 -> matrix -> next = 0;

    camara4->pos.x = 1.0;
    camara4->pos.y = 1.0;
    camara4->pos.z = 1.0;  

    glMatrixMode(GL_MODELVIEW);

    gluLookAt(1.0, 1.0, 1.0,
              5.0, 5.0, 5.0,
              0.0, 1.0, 1.0 );

    glGetFloatv(GL_MODELVIEW_MATRIX, camara4->matrix->matriz_16);
    glLoadIdentity();

    camara4 -> left =   -0.1f;
    camara4 -> right =   0.1f;
    camara4 -> bottom = -0.1f;
    camara4 -> top =     0.1f;
    camara4 -> near=     0.1f;
    camara4 -> far =     1000.0f;

    camara4 -> prev =   camara3;
    camara4 -> next =   camara1;

    camara4 -> ortho_x_min = KG_ORTHO_X_MIN_INIT;
    camara4 -> ortho_x_max = KG_ORTHO_X_MAX_INIT;
    camara4 -> ortho_y_min = KG_ORTHO_Y_MIN_INIT;
    camara4 -> ortho_y_max = KG_ORTHO_Y_MAX_INIT;

/*
    printf("%f\t%f\t%f\t%f\n", camara4->matrix->matriz_16[0], camara4->matrix->matriz_16[4], camara4->matrix->matriz_16[8],  camara4->matrix->matriz_16[12]);
    printf("%f\t%f\t%f\t%f\n", camara4->matrix->matriz_16[1], camara4->matrix->matriz_16[5], camara4->matrix->matriz_16[8],  camara4->matrix->matriz_16[13]);
    printf("%f\t%f\t%f\t%f\n", camara4->matrix->matriz_16[2], camara4->matrix->matriz_16[6], camara4->matrix->matriz_16[10], camara4->matrix->matriz_16[14]);
    printf("%f\t%f\t%f\t%f\n", camara4->matrix->matriz_16[3], camara4->matrix->matriz_16[7], camara4->matrix->matriz_16[11], camara4->matrix->matriz_16[15]);
*/
    /****************************************/
    /*Definition of the background color*/
    glClearColor(KG_COL_BACK_R, KG_COL_BACK_G, KG_COL_BACK_B, KG_COL_BACK_A);
}


/** MAIN FUNCTION **/
int main(int argc, char** argv) {

    /*First of all, print the help information*/
    print_help();

    /* glut initializations */
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB|GLUT_DEPTH|GLUT_DOUBLE);
    glutInitWindowSize(KG_WINDOW_WIDTH, KG_WINDOW_HEIGHT);
    glutInitWindowPosition(KG_WINDOW_X, KG_WINDOW_Y);
    glutCreateWindow(KG_WINDOW_TITLE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); // poligonos opacos

    glEnable(GL_DEPTH_TEST); //Test de profundidad, z-buffer

    if(flat_smooth)                 
        glShadeModel(GL_SMOOTH);
    else                            
        glShadeModel(GL_FLAT);


    /* set the callback functions */
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);

    /*  FUNCIONES ESPECIALES:
    */
    glutSpecialFunc(specialKeys);

    /* this initialization has to be AFTER the creation of the window */
    initialization();

    /* start the main loop */
    glutMainLoop();
    return 0;
}
