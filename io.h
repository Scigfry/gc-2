#ifndef IO_H
#define IO_H

void keyboard(unsigned char key, int x, int y);
/*	Función de teclado especial añadida
*/
void specialKeys(int key, int x, int y);
void print_help();
void guardarCambios();
void position();
void cameraPosition();

#endif // IO_H
