#include "definitions.h"
#include "load_obj.h"
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern object3d * _first_object;
extern object3d * _selected_object;

extern GLdouble _ortho_x_min,_ortho_x_max;
extern GLdouble _ortho_y_min,_ortho_y_max;
extern GLdouble _ortho_z_min,_ortho_z_max;


//  Camaras:

extern camera_list * camara1;
extern camera_list * camara2;
extern camera_list * camara3;
extern camera_list * camara4;
camera_list * cam_aux;

//  Transformaciones:

int trans_obj =     1;  //  O, o
 
int trasladar =     0;  //  M, m
int escalar =       0;  //  T, t
int rotar =         0;  //  B, b
int mundo =         0;  //  L, l
int objeto =        1;  //  G, g


 
//  Camara:

int trans_cam =     0;  //  K, k

int trasl_camara =  0;  //  M, m
int vol_vision =    0;  //  T, t
int rot_camara =    0;  //  B, b
int vuelo =         1;  //  L, l
int analisis =      0;  //  G, g
int pers_paral =    0;  //  P, p
int cam_obj =       0;  //  C


// Luces:

// Dirección del sol
extern float sun_dir[];

//Posición de la bombilla
extern float bulb_pos[]; 

//Ángulo del foco de la cámara
extern float cam_angle;

//Ángulo del foco del objeto
extern float focus_angle;

// Posición y ángulo de la bombilla

int flat_smooth = FALSE; // F12
int luz =         FALSE; //  F9
int trans_luz =       0; //  A, a

int luces[] = {FALSE, FALSE, FALSE, FALSE};
int nLuces = 4;
int luz_esc = -1;
int mov_bulb = FALSE;
int rot_sun = FALSE;

// Lo usamos para calcular números aleatorios para los materiales
void rand10_100(float *n){
    float num;
    time_t t;
    srand((unsigned) time(&t));
    for(int i = 0; i < 10; i++){
        num = (float)(rand() % 100) / 100;
        n[i] = num;
    }
}


void printMatrix(GLfloat* m){
    
    printf("%f\t%f\t%f\t%f\n", m[0], m[4], m[8],  m[12]);
    printf("%f\t%f\t%f\t%f\n", m[1], m[5], m[8],  m[13]);
    printf("%f\t%f\t%f\t%f\n", m[2], m[6], m[10], m[14]);
    printf("%f\t%f\t%f\t%f\n", m[3], m[7], m[11], m[15]);
   
}


void position() {

    printf("Mostrando matriz pos\n");
    printMatrix(_selected_object-> first -> matriz_16);
}

void cameraPosition() {

    printf("Mostrando camara1:\n");
    printMatrix(camara1 -> matrix -> matriz_16);
    printf("Left:%f\tRight:%f\tTop:%f\n", camara1 -> left, camara1 -> right, camara1 -> top);
    printf("Botm:%f\tFar:%f\tNear:%f\n", camara1 -> bottom, camara1 -> far, camara1 -> near);
}





/**
 * @brief This function just prints information about the use
 * of the keys
 */
void status(){

    printf("\n");
    
    printf("--------------------------------------------\n");
    printf("\t\tSTATUS\n");
    printf("--------------------------------------------\n");

    if(trans_obj){
        if(cam_obj){ printf("CAMARA OBJETO, CAMBIOS AL OBJETO:%d\n\n", trans_obj);   
        } else printf("TRANSFORMACIONES:%d\n\n", trans_obj);   
        printf("\tTrasladar:\t%d\n", trasladar);
        printf("\tEscalar:\t%d\n", escalar);
        printf("\tRotar:\t\t%d\n", rotar);
        printf("\tMundo:\t\t%d\n", mundo);
        printf("\tObjeto:\t\t%d\n", objeto);

        printf("\n");
    }

    if(trans_cam){
        printf("CAMARA: %d\n\n", trans_cam);
        printf("\tModo Analisis:\t\t%d\n", analisis);
        printf("\tModo Vuelo:\t\t%d\n", vuelo);
        printf("\tVolumen Vision:\t\t%d\n", vol_vision);
        printf("\tRotar Camara:\t\t%d\n", rot_camara);
        printf("\tTrasladar Camara:\t%d\n", trasl_camara);
        printf("\tPersp./Paral:\t\t%d\n", pers_paral);
        printf("\tCamara objeto:\t\t%d\n", cam_obj);
    }
    
    printf("\n");
    
    printf("LUCES: %d\n\n", luz);

    if(luz){
        printf("\tBombilla:\t");if(luces[1])printf("Encendida\n");else printf("Apagada\n");
        printf("\tSol:\t\t");if(luces[0])printf("Encendido\n");else printf("Apagado\n");
        printf("\tCamara:\t\t");if(luces[2])printf("Encendida\n");else printf("Apagada\n");
        printf("\tObjeto:\t\t");if(luces[3])printf("Encendido\n");else printf("Apagado\n");
    }

    if(trans_luz){
        printf("TRANSFORMACIONES A LUCES: %d\n\n", trans_luz);

        printf("\tLuz escogida:\t\t%d\n", luz_esc);
        printf("\tMover bombilla:\t\t%d\n", mov_bulb);
        printf("\tGirar sol:\t\t%d\n", rot_sun);

    }

    printf("--------------------------------------------\n");

    //cameraPosition();

}

void print_help(){

    //printf("KbG Irakasgaiaren Praktika. Programa honek 3D objektuak \n");
    //printf("aldatzen eta bistaratzen ditu.  \n\n");
    printf("\n\n");
    printf("----------------------\n");
    printf("FUNCIONES PRINCIPALES \n");
    printf("?\t\t Imprimir esta información \n");
    printf("ESC\t\t Salir del programa \n");
    printf("F f\t\t Cargar un objeto\n");
    printf("TAB\t\t Escoger uno de los objetos cargados\n");
    printf("DEL\t\t Eliminar el objeto seleccionado\n");
    printf("CTRL -\t\t Aumentar el volumen de visión\n");
    printf("CTRL +\t\t Reducir el volumen de visión\n");
    printf("F9\t\t Activar/desactivar iluminación\n");

    if(luz){
        printf("\n------------LUCES ENCENDIDAS------------\n");
        printf("F1-F8\t\t Encender/apagar fuente de luz\n");
        printf("F12\t\t Cambiar tipo de iluminación (flat-smooth)\n");
    }
    
    if(trans_obj){

        printf("\n------------OBJETO------------n");

        printf("\nTipo de Transformación\n");
        printf("M m \t\tActivar Traslacion\n");
        printf("B b \t\tActivar Rotacion\n");
        printf("T t \t\tActivar Escalado\n");
        printf("+ \t\tEscalar + en todos los ejes\n");
        printf("- \t\tEscalar - en todos los ejes\n");

        printf("\nSistema de referencia\n");
        printf("G g \t\tSistema de referencia del Mundo\n");
        printf("L l \t\tSistema de referencia del Objeto\n");
        
        if(cam_obj == FALSE){
            printf("\nElemento a transformar\n");

            printf("K k \t\tAplicar transformaciones a la cámara actual\n");
            printf("A a \t\tAplicar transformaciones a la luz seleccionada\n");
        }

        printf("----------------------");

    } else if(trans_cam){

        printf("\n------------CÁMARA------------\n");
        printf("c \t\tCambiar de cámara\n");
        printf("C \t\tCámara del objeto\n");

        printf("\nTipo de Transformación\n");
        printf("M m \t\tTraslaciones a la cámara\n");
        printf("B b \t\tRotaciones a la cámara\n");
        printf("T t \t\tCambio de volumen de visión\n");

        printf("\nModo de cámara\n");
        printf("G g \t\tCámara en modo análisis\n");
        printf("L l \t\tCámara en modo vuelo\n");
        printf("P p \t\tCámara en modo perpectiva\n");
        
        printf("\nElemento a transformar\n");
        printf("O o \t\tAplicar transformaciones al objeto seleccionado\n");
        printf("A a \t\tAplicar transformaciones a la luz seleccionada\n");
        printf("----------------------");


    } else if(trans_luz){
        printf("\n------------LUZ------------\n");

        printf("1-8\t\t Seleccionar la fuente de luz\n");
        printf("0\t\t Asignar tipo de fuente (4-8)\n");


        printf("\nTipo de Transformación\n");
        printf("M m \t\tActivar Traslacion (solo para bombilla)\n");
        printf("B b \t\tActivar Rotacion (solo para el sol)\n");
        printf("+ \t\tIncrementar ángulo de apertura (focos)\n");
        printf("- \t\tDecrementar ángulo de apertura (focos)\n");
       
        printf("\nElemento a transformar\n");
        printf("O o \t\tAplicar transformaciones al objeto seleccionado\n");
        printf("K k \t\tAplicar transformaciones a la cámara actual\n");
        printf("----------------------");
    }


    printf("\n");       
}

/**
 * @brief Callback function to control the basic keys
 * @param key Key that has been pressed
 * @param x X coordinate of the mouse pointer when the key was pressed
 * @param y Y coordinate of the mouse pointer when the key was pressed
 */



void printMatrixOfObject(object3d* o){

    printf("Mostrando matriz de objeto o:\n");
    printf("%f %f %f %f\n", o->first->matriz_16[0], o->first->matriz_16[4], o->first->matriz_16[8],  o->first->matriz_16[12]);
    printf("%f %f %f %f\n", o->first->matriz_16[1], o->first->matriz_16[5], o->first->matriz_16[9],  o->first->matriz_16[13]);
    printf("%f %f %f %f\n", o->first->matriz_16[2], o->first->matriz_16[6], o->first->matriz_16[10], o->first->matriz_16[14]);
    printf("%f %f %f %f\n", o->first->matriz_16[3], o->first->matriz_16[7], o->first->matriz_16[11], o->first->matriz_16[15]);
}


void guardarCambios(){
    if(trans_obj){
        //Actualizamos la matriz del objeto
        matrix_list* newptr = malloc(sizeof(matrix_list));
        newptr->next = _selected_object->first;
        glGetFloatv(GL_MODELVIEW_MATRIX, newptr->matriz_16);
        _selected_object->first = newptr;

        //Actualizamos la cámara del objeto
        glLoadIdentity();
        _selected_object->camera->pos.x = _selected_object->first->matriz_16[12];
        _selected_object->camera->pos.y = _selected_object->first->matriz_16[13];
        _selected_object->camera->pos.z = _selected_object->first->matriz_16[14];
        //Colocamos la camara en la posicion del objeto
        gluLookAt(  _selected_object->first->matriz_16[12], _selected_object->first->matriz_16[13], _selected_object->first->matriz_16[14],
                    _selected_object->first->matriz_16[12] - _selected_object->first->matriz_16[8], 
                    _selected_object->first->matriz_16[13] - _selected_object->first->matriz_16[9],
                    _selected_object->first->matriz_16[14] - _selected_object->first->matriz_16[10],
                    _selected_object->first->matriz_16[4], _selected_object->first->matriz_16[5], _selected_object->first->matriz_16[6]);
        //Guardamos la matriz activa en la camara
        glGetFloatv(GL_MODELVIEW_MATRIX, _selected_object->camera->matrix->matriz_16);


    } else if(trans_cam){
        matrix_list* newptr = malloc(sizeof(matrix_list));
        glGetFloatv(GL_MODELVIEW_MATRIX, newptr->matriz_16);
        newptr->next =  camara1->matrix;
        camara1->matrix = newptr;
    }  

}

void deshacer(void){
    
    if(mundo == FALSE && objeto == FALSE){
        printf("No se puede deshacer\n");
    } else {
        if(_selected_object->first->next != NULL){
            printf("Deshacer\n");
            glMatrixMode(GL_MODELVIEW);
            matrix_list* erase = _selected_object->first;    
            _selected_object->first = _selected_object->first->next;
            glLoadMatrixf(_selected_object->first->matriz_16);
            free(erase);
        } else {
            printf("No se puede deshacer más\n");
        }
    }
}


void anadirCamara() {
    camera_list* cam_new = malloc(sizeof(camera_list));
    cam_new -> matrix = malloc(sizeof(matrix_list));

    cam_new->matrix->next = 0;

    cam_new->pos.x = 0.0;
    cam_new->pos.y = 0.0;
    cam_new->pos.z = 0.0;

    /* Creo que esto no es necesario */
    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity(); 
    glGetFloatv(GL_MODELVIEW_MATRIX, cam_new->matrix->matriz_16);

    cam_new -> left =   -0.1f;
    cam_new -> right =   0.1f;
    cam_new -> bottom = -0.1f;
    cam_new -> top =     0.1f;
    cam_new -> near=     0.1f;
    cam_new -> far =     1000.0f;

    cam_new -> ortho_x_min = KG_ORTHO_X_MIN_INIT;
    cam_new -> ortho_x_max = KG_ORTHO_X_MAX_INIT;
    cam_new -> ortho_y_min = KG_ORTHO_Y_MIN_INIT;   
    cam_new -> ortho_y_max = KG_ORTHO_Y_MAX_INIT;


    cam_aux = camara1;



    int complete = FALSE;
    while(!complete){
        if(cam_aux->id > cam_aux->next->id){
            cam_new->id = cam_aux->id + 1;
            cam_new->prev = cam_aux;
            cam_new->next = cam_aux->next;
            cam_aux->next->prev = cam_new;
            cam_aux->next = cam_new;
            complete = TRUE;
        } else
            cam_aux = cam_aux->next;
    }
}

void objectCamera() {

        camera_list* cam_obj_aux = malloc(sizeof(camera_list));
        cam_obj_aux -> matrix = malloc(sizeof(matrix_list));

        cam_obj_aux -> pos.x = _selected_object -> first -> matriz_16[12];
        cam_obj_aux -> pos.y = _selected_object -> first -> matriz_16[13];
        cam_obj_aux -> pos.z = _selected_object -> first -> matriz_16[14];


        gluLookAt(  _selected_object -> first -> matriz_16[12], 
                    _selected_object -> first -> matriz_16[13], 
                    _selected_object -> first -> matriz_16[14],
                    _selected_object -> first -> matriz_16[12] - _selected_object -> first -> matriz_16[8],
                    _selected_object -> first -> matriz_16[13] - _selected_object -> first -> matriz_16[9],
                    _selected_object -> first -> matriz_16[14] - _selected_object -> first -> matriz_16[10],
                    _selected_object -> first -> matriz_16[4],
                    _selected_object -> first -> matriz_16[5],
                    _selected_object -> first -> matriz_16[6]);

        glGetFloatv(GL_MODELVIEW_MATRIX, cam_obj_aux -> matrix -> matriz_16);

        cam_obj_aux -> left = -0.1;
        cam_obj_aux -> right = 0.1;
        cam_obj_aux -> bottom = -0.1;
        cam_obj_aux -> top = 0.1;
        cam_obj_aux -> near = 0.1;
        cam_obj_aux -> far = 1000.0;

        cam_obj_aux -> prev =   NULL;
        cam_obj_aux -> next =   NULL;
        cam_obj_aux -> matrix -> next =   NULL;

        //printMatrix(cam_obj_aux->matrix->matriz_16);

        cam_obj_aux -> ortho_x_min = KG_ORTHO_X_MIN_INIT;
        cam_obj_aux -> ortho_x_max = KG_ORTHO_X_MAX_INIT;
        cam_obj_aux -> ortho_y_min = KG_ORTHO_Y_MIN_INIT;
        cam_obj_aux -> ortho_y_max = KG_ORTHO_Y_MAX_INIT;

        _selected_object -> camera = cam_obj_aux;

        printf("Matriz Cambio Sistema de Referencia tras camara del objeto:\n");
        //cameraPosition();
    
}

void objectLight() {

    float materiales[10];

    rand10_100(materiales);

    printf("%f %f %f %f %f %f %f %f %f %f\n", materiales[0], materiales[1], materiales[2], materiales[3], materiales[4], materiales[5], materiales[6], materiales[7], materiales[8], materiales[9]);

    GLfloat* mat_specular = malloc(sizeof(GLfloat));
    GLfloat* mat_ambient = malloc(sizeof(GLfloat));
    GLfloat* mat_diffuse = malloc(sizeof(GLfloat));
    GLfloat* mat_shininess = malloc(sizeof(GLfloat));

    mat_specular[0] = materiales[0]; mat_specular[1] = materiales[1]; mat_specular[2] = materiales[2]; mat_specular[3] = 1.0;
    mat_ambient[0] = materiales[3]; mat_ambient[1] = materiales[5]; mat_ambient[2] = materiales[6]; mat_ambient[3] = 1.0;
    mat_diffuse[0] = materiales[6]; mat_diffuse[1] = materiales[8]; mat_diffuse[2] = materiales[9]; mat_diffuse[3] = 1.0;
    mat_shininess[0] = materiales[9];

    _selected_object->mat_specular = mat_specular;
    _selected_object->mat_ambient = mat_ambient;
    _selected_object->mat_diffuse = mat_diffuse;
    _selected_object->mat_shininess = mat_shininess;
}

/* Modo de uso de translate, scalate y rotate
 * Usan como input el valor de la tecla que se haya usado en ese modo
 * Después, comprueban a qué están haciendo el cambio y en base a ello, lo aplican */


void translate(int mode){

    if (trans_obj){

        glMatrixMode(GL_MODELVIEW);
        
        if (objeto) 
            glLoadMatrixf(_selected_object->first->matriz_16);
        else if (mundo)  
            glLoadIdentity();


        switch (mode){
            case UP:    glTranslatef(0,KG_STEP_MOVE,0);  break;
            case DOWN:  glTranslatef(0,-KG_STEP_MOVE,0); break;
            case LEFT:  glTranslatef(-KG_STEP_MOVE,0,0); break;
            case RIGHT: glTranslatef(KG_STEP_MOVE,0,0);  break;
            case AVPAG: glTranslatef(0,0,KG_STEP_MOVE);  break;
            case REPAG: glTranslatef(0,0,-KG_STEP_MOVE); break;
            default: break;
        }

        if(mundo) glMultMatrixf(_selected_object->first->matriz_16); 

        //  Guardar cambios

        guardarCambios();

        //position();
        //cameraPosition();

    } else if(trans_cam){

        printf("TRANSLATE: CAMERA OBJETO\n");

        if (vuelo) {

            printf("\tMODO VUELO\n");
            
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();


            switch (mode){
                case UP:    glTranslatef(0,KG_STEP_MOVE,0);  break;
                case DOWN:  glTranslatef(0,-KG_STEP_MOVE,0); break;
                case LEFT:  glTranslatef(-KG_STEP_MOVE,0,0); break;
                case RIGHT: glTranslatef(KG_STEP_MOVE,0,0);  break;
                case AVPAG: glTranslatef(0,0,KG_STEP_MOVE);  break;
                case REPAG: glTranslatef(0,0,-KG_STEP_MOVE); break;
                default: break;
            }
        
            printf("Camara BEFORE POS:\t(%f, %f, %f)\n", camara1 -> pos.x, camara1-> pos.y, camara1 -> pos.z);

            switch (mode){
                case UP:    camara1 -> pos.y +=  KG_STEP_MOVE; break;
                case DOWN:  camara1 -> pos.y += -KG_STEP_MOVE; break;
                case LEFT:  camara1 -> pos.x += -KG_STEP_MOVE; break;
                case RIGHT: camara1 -> pos.x +=  KG_STEP_MOVE; break;
                case AVPAG: camara1 -> pos.z +=  KG_STEP_MOVE; break;
                case REPAG: camara1 -> pos.z += -KG_STEP_MOVE; break;
                default: break;
            }

            printf("Camara AFTER POS:\t(%f, %f, %f)\n", camara1 -> pos.x, camara1 -> pos.y, camara1 -> pos.z);
            
            glMatrixMode(GL_MODELVIEW);
            glMultMatrixf(camara1->matrix->matriz_16);
            glutPostRedisplay();
            guardarCambios();
            //position();
            //cameraPosition();
        
        } else {

            printf("camara1.z:%f Z obj.z:%f\n",camara1 -> pos.z,_selected_object -> first -> matriz_16[14]);
            
            float dz = -camara1->pos.z - _selected_object -> first -> matriz_16[14];
            if (dz < 0.0f) dz = -dz; 
                
            printf("Dist z:%f\n",dz);

            //position();
            //cameraPosition();

            if(dz > KG_STEP_MOVE){

                glLoadIdentity();
            

                switch (mode){
                    case UP:    glTranslatef(0,KG_STEP_MOVE,0);  break;
                    case DOWN:  glTranslatef(0,-KG_STEP_MOVE,0); break;
                    case LEFT:  glTranslatef(-KG_STEP_MOVE,0,0); break;
                    case RIGHT: glTranslatef(KG_STEP_MOVE,0,0);  break;
                    case AVPAG: glTranslatef(0,0,KG_STEP_MOVE);  break;
                    case REPAG: glTranslatef(0,0,-KG_STEP_MOVE); break;
                    default: break;
                }

                camara1->pos.z += KG_STEP_MOVE;

                glMatrixMode(GL_MODELVIEW);
                glMultMatrixf(camara1->matrix->matriz_16);
                guardarCambios();
                printf("camara1->pos.z: %f\n",camara1->pos.z);
                
                //position();
                //cameraPosition();

            } else { 
                
                printf("No nos podemos mover mas adelante\n");
                printf("DistanciaZ:%f <= Movimiento:%f\n",dz,KG_STEP_MOVE);
            }
        }
    } else if(trans_luz){
        if(luz_esc == 1){
            switch (mode){
                    case UP:    bulb_pos[1] += KG_STEP_MOVE; break;
                    case DOWN:  bulb_pos[1] -= KG_STEP_MOVE; break;
                    case LEFT:  bulb_pos[0] -= KG_STEP_MOVE; break;
                    case RIGHT: bulb_pos[0] += KG_STEP_MOVE; break;
                    case AVPAG: bulb_pos[2] += KG_STEP_MOVE; break;
                    case REPAG: bulb_pos[2] -= KG_STEP_MOVE; break;
                    default: break;
                }
            glLightfv(GL_LIGHT1, GL_POSITION, bulb_pos);  
            //
        } else if(luz_esc == 2, luz_esc == 3, luz_esc == 4)
            printf("Esta luz no se puede mover\n");
    }
}


void scalate(int mode){
    if(trans_obj){
        glMatrixMode(GL_MODELVIEW);
        if(objeto) //Se carga el objeto
            glLoadMatrixf(_selected_object->first->matriz_16);
        else if(mundo) //Se carga la matriz de identidad para operar respecto al punto de origen
            glLoadIdentity();

        switch (mode){
            case UP:    glScalef(1,SCALE_UP_UNIT,1);   break;
            case DOWN:  glScalef(1,SCALE_DOWN_UNIT,1); break;
            case LEFT:  glScalef(SCALE_DOWN_UNIT,1,1); break;
            case RIGHT: glScalef(SCALE_UP_UNIT,1,1);   break;
            case AVPAG: glScalef(1,1,SCALE_UP_UNIT);   break;
            case REPAG: glScalef(1,1,SCALE_DOWN_UNIT); break;

            case UPSCALE:   glScalef(SCALE_UP_UNIT,SCALE_UP_UNIT,SCALE_UP_UNIT);        break;
            case DOWNSCALE: glScalef(SCALE_DOWN_UNIT,SCALE_DOWN_UNIT,SCALE_DOWN_UNIT);  break;
            default: break;
        }

        if(mundo) //Lo multiplicamos por la matriz del objeto para rotarlo
            glMultMatrixf(_selected_object->first->matriz_16);

        guardarCambios();
        //position();
    } else if(trans_cam){
        switch (mode){
            case UP:    camara1->top = camara1->top * SCALE_UP_UNIT; camara1->bottom = camara1->bottom * SCALE_UP_UNIT;  break;
            case DOWN:  camara1->top = camara1->top * SCALE_DOWN_UNIT; camara1->bottom = camara1->bottom * SCALE_DOWN_UNIT; break;
            case LEFT:  camara1->left = camara1->left * SCALE_UP_UNIT; camara1->right = camara1->right * SCALE_UP_UNIT; break;
            case RIGHT: camara1->left = camara1->left * SCALE_DOWN_UNIT; camara1->right = camara1->right * SCALE_DOWN_UNIT; break;
            case AVPAG: camara1->far = camara1->far * SCALE_UP_UNIT; camara1->near = camara1->near * SCALE_UP_UNIT;  break;
            case REPAG: camara1->far = camara1->far * SCALE_DOWN_UNIT; camara1->near = camara1->near * SCALE_DOWN_UNIT;  break;
            default: break;
        }
    }
}

void rotate(int mode){


    if (trans_obj){

        glMatrixMode(GL_MODELVIEW);
        if(objeto) //Se carga el objeto
            glLoadMatrixf(_selected_object->first->matriz_16);
        else if(mundo) //Se carga la matriz de identidad para operar respecto al punto de origen
            glLoadIdentity();

        switch (mode){
            case UP:    glRotatef(KG_STEP_ROTATE,1,0,0);  break;
            case DOWN:  glRotatef(KG_STEP_ROTATE,-1,0,0); break;
            case LEFT:  glRotatef(KG_STEP_ROTATE,0,-1,0); break;
            case RIGHT: glRotatef(KG_STEP_ROTATE,0,1,0);  break;
            case AVPAG: glRotatef(KG_STEP_ROTATE,0,0,1);  break;
            case REPAG: glRotatef(KG_STEP_ROTATE,0,0,-1); break;
            default: break;
        }

        if(mundo) glMultMatrixf(_selected_object->first->matriz_16);

        guardarCambios();
        
        //position();

    } else if(trans_cam){

        if (vuelo) {

            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();

            switch (mode){
                case UP:    glRotatef(KG_STEP_ROTATE,1,0,0);  break;
                case DOWN:  glRotatef(KG_STEP_ROTATE,-1,0,0); break;
                case LEFT:  glRotatef(KG_STEP_ROTATE,0,-1,0); break;
                case RIGHT: glRotatef(KG_STEP_ROTATE,0,1,0);  break;
                case AVPAG: glRotatef(KG_STEP_ROTATE,0,0,1);  break;
                case REPAG: glRotatef(KG_STEP_ROTATE,0,0,-1); break;
                default: break;
            }

            glMatrixMode(GL_MODELVIEW);
            glMultMatrixf(camara1 -> matrix -> matriz_16);

            guardarCambios();
            //position();
            //cameraPosition();
        
        } else {

            matrix_list * mat = malloc(sizeof (matrix_list));
            
            printf("Posde la camara\n");
            printf("Posde la camara X %f Y %f Z %f\n",camara1 -> pos.x, camara1 -> pos.y, camara1 -> pos.z);
            
            mat -> matriz_16[0] = camara1 -> matrix -> matriz_16[0];
            mat -> matriz_16[1] = camara1 -> matrix -> matriz_16[4];
            mat -> matriz_16[2] = camara1 -> matrix -> matriz_16[8];  
            mat -> matriz_16[3] = 0.0;


            mat -> matriz_16[4] = camara1 -> matrix -> matriz_16[1];
            mat -> matriz_16[5] = camara1 -> matrix -> matriz_16[5];  
            mat -> matriz_16[6] = camara1 -> matrix -> matriz_16[9];  
            mat -> matriz_16[7] = 0.0;


            mat -> matriz_16[8]  = camara1 -> matrix -> matriz_16[2];  
            mat -> matriz_16[9]  = camara1 -> matrix -> matriz_16[6];
            mat -> matriz_16[10] = camara1 -> matrix -> matriz_16[10];
            mat -> matriz_16[11] = 0.0;
            mat -> matriz_16[12] = camara1 -> pos.x;
            mat -> matriz_16[13] = camara1 -> pos.y;
            mat -> matriz_16[14] = camara1 -> pos.z;
            mat -> matriz_16[15] = 1.0;
                            
            glLoadIdentity();

            glMatrixMode(GL_MODELVIEW);

            glTranslatef(   _selected_object -> first -> matriz_16[12],
                            _selected_object -> first -> matriz_16[13],
                            _selected_object -> first -> matriz_16[14]);
            
            switch (mode){
                case UP:    glRotatef(KG_STEP_ROTATE,1,0,0);  break;
                case DOWN:  glRotatef(KG_STEP_ROTATE,-1,0,0); break;
                case LEFT:  glRotatef(KG_STEP_ROTATE,0,-1,0); break;
                case RIGHT: glRotatef(KG_STEP_ROTATE,0,1,0);  break;
                case AVPAG: glRotatef(KG_STEP_ROTATE,0,0,1);  break;
                case REPAG: glRotatef(KG_STEP_ROTATE,0,0,-1); break;
                default: break;
            }

            glTranslatef(   -_selected_object -> first -> matriz_16[12],
                            -_selected_object -> first -> matriz_16[13],
                            -_selected_object -> first -> matriz_16[14]);
            
            glMultMatrixf(mat -> matriz_16);
        
            
            printf("Pos de mat\n");
            printf("Pos de la camara X %f Y %f Z %f\n",mat -> matriz_16[12], camara1 -> pos.y, camara1 -> pos.z);
            printf("Pos de la camara\n");
            printf("Pos de la camara X %f Y %f Z %f\n",camara1 -> pos.x, camara1 -> pos.y, camara1 -> pos.z);
            
            printf("Mat = Inversa de la MCSR:\n");

            printf("%f %f %f %f\n",mat -> matriz_16[0], mat -> matriz_16[4], mat -> matriz_16[8],  mat -> matriz_16[12]);
            printf("%f %f %f %f\n",mat -> matriz_16[1], mat -> matriz_16[5], mat -> matriz_16[9],  mat -> matriz_16[13]);
            printf("%f %f %f %f\n",mat -> matriz_16[2], mat -> matriz_16[6], mat -> matriz_16[10], mat -> matriz_16[14]);
            printf("%f %f %f %f\n",mat -> matriz_16[3], mat -> matriz_16[7], mat -> matriz_16[11], mat -> matriz_16[15]);  
                
            
            glMatrixMode(GL_MODELVIEW);
            glGetFloatv(GL_MODELVIEW_MATRIX,mat->matriz_16);
            camara1->pos.x = mat->matriz_16[12];
            camara1->pos.y = mat->matriz_16[13];
            camara1->pos.z = mat->matriz_16[14];

        
            camara1->matrix->matriz_16[0] = mat->matriz_16[0];
            camara1->matrix->matriz_16[1] = mat->matriz_16[4];
            camara1->matrix->matriz_16[2] = mat->matriz_16[8];  
            camara1->matrix->matriz_16[3] = 0.0;


            camara1->matrix->matriz_16[4] = mat->matriz_16[1];
            camara1->matrix->matriz_16[5] = mat->matriz_16[5];  
            camara1->matrix->matriz_16[6] = mat->matriz_16[9];  
            camara1->matrix->matriz_16[7] = 0.0;


            camara1->matrix->matriz_16[8]  = mat->matriz_16[2];  
            camara1->matrix->matriz_16[9]  = mat->matriz_16[6];
            camara1->matrix->matriz_16[10] = mat->matriz_16[10];
            camara1->matrix->matriz_16[11] = 0.0;


            camara1->matrix->matriz_16[12] = -((mat->matriz_16[0]*mat->matriz_16[12])+(mat->matriz_16[1]*mat->matriz_16[13]) + (mat->matriz_16[2]*mat->matriz_16[14]));
            camara1->matrix->matriz_16[13] = -((mat->matriz_16[4]*mat->matriz_16[12])+(mat->matriz_16[5]*mat->matriz_16[13]) + (mat->matriz_16[6]*mat->matriz_16[14]));              
            camara1->matrix->matriz_16[14] = -((mat->matriz_16[8]*mat->matriz_16[12])+(mat->matriz_16[9]*mat->matriz_16[13]) + (mat->matriz_16[10]*mat->matriz_16[14]));
            camara1->matrix->matriz_16[15] = 1.0;

            glLoadMatrixf(camara1->matrix->matriz_16); 
            guardarCambios();
            //position();
            //cameraPosition();

        }
    
    } else if(trans_luz){
        if(luz_esc == 2){
            switch (mode){
                case UP:    sun_dir[1] += 0.1; break;
                case DOWN:  sun_dir[1] -= 0.1; break;
                case LEFT:  sun_dir[0] -= 0.1; break;
                case RIGHT: sun_dir[0] += 0.1; break;
                case AVPAG: sun_dir[2] += 0.1; break;
                case REPAG: sun_dir[2] -= 0.1; break;
                default: break;
            }
            glLightfv(GL_LIGHT0, GL_POSITION, sun_dir);  
            //
        } else if(luz_esc == 1, luz_esc == 3, luz_esc == 4, luz_esc == 5)
            printf("Esta luz no se puede mover\n");
        
    }
}


void keyboard(unsigned char key, int x, int y) {

    char* fname = malloc(sizeof (char)*128); /* Note that scanf adds a null character at the end of the vector*/
    int read = 0;
    object3d *auxiliar_object = 0;
    GLdouble wd,he,midx,midy;

    matrix_list* coor_matrix = malloc(sizeof(matrix_list));

    switch (key) {

        // Objeto
        case 'o': case 'O':
            if(!trans_obj){
                trans_obj = TRUE;
                trans_cam = FALSE;
                trans_luz = FALSE;
            }
            status();
            break;

        // Cámara
        case 'k': case 'K':
            if(cam_obj){
                printf("Ahora mismo estamos en la cámara del objeto, y por ende, no permitimos cambios de este tipo\n");
            } else {               
                if(!trans_cam){
                    trans_obj = FALSE;
                    trans_cam = TRUE;
                    trans_luz = FALSE;
                }
            }
            status();
            break;

        // Luz
        case 'a': case 'A':
            if(cam_obj){
                printf("Ahora mismo estamos en la cámara del objeto, y por ende, no permitimos cambios de este tipo\n");
            } else { 
                if(!trans_luz){
                    trans_obj = FALSE;
                    trans_cam = FALSE;
                    trans_luz = TRUE;
                }
            }
            status();
            break;

        //  Trasladar:
        case 'm': case 'M':
            
            //  Caso de Transformación:
            if (trans_obj){
                if(trasladar)  
                    trasladar = FALSE; 
                else {
                    trasladar = TRUE; 
                    rotar     = FALSE;
                    escalar   = FALSE;
                }
            } else if(trans_cam){
            //  Caso de Camara:
            if(trasl_camara)
                    trasl_camara = FALSE;
                else{
                    trasl_camara = TRUE;
                    rot_camara   = FALSE;
                    vol_vision   = FALSE;
                }
            } else if(trans_luz){
                if(mov_bulb)
                    mov_bulb = FALSE;
                else{
                    mov_bulb = TRUE;
                    rot_sun = FALSE;
                }
            }
            status();
            break;
        
        //  Rotar:
        case 'b': case 'B': 
            if (trans_obj){
                if(rotar)
                    rotar     = FALSE;
                else {
                    rotar     = TRUE;
                    trasladar = FALSE;
                    escalar   = FALSE;
                }
            } else if(trans_cam){
                if (rot_camara)
                    rot_camara    = FALSE;
                else{
                    rot_camara    = TRUE;
                    trasl_camara  = FALSE;
                    vol_vision    = FALSE;
                }
            } else if(trans_luz){
                if(rot_sun)
                    rot_sun = FALSE;
                else{
                    rot_sun = TRUE;
                    mov_bulb = FALSE;
                }
            }
            status();
            break;

        //  Escalar:
        case 't': case 'T': 
            if (trans_obj){
                if(escalar)
                    escalar   = FALSE;
                else {
                    escalar   = TRUE;
                    trasladar = FALSE;
                    rotar     = FALSE;
                }
            } else if(trans_cam){
                if(vol_vision)
                    vol_vision   = FALSE;
                else {
                    vol_vision   = TRUE;
                    trasl_camara = FALSE;
                    rot_camara   = FALSE;
                }
            } else if(trans_luz){
                printf("Puto");
            }
            status();
            break;

        
        //  Transformaciones Globales/  Modo Analisis:
        
        case 'g': case 'G': 
            
            if (trans_obj){

                if(objeto){
                    mundo   = TRUE; 
                    objeto  = FALSE;             
                } 
                    
            } else if(trans_cam){
                
                if (vuelo){

                    printf("Usted acaba de ACTIVAR el modo de analisis, estamos mirando al objeto.\n");
                    matrix_list * mat = malloc(sizeof (matrix_list));                   
                    
                    //glLoadIdentity();              
             
                    gluLookAt(  camara1 -> pos.x, 
                                camara1 -> pos.y, 
                                camara1 -> pos.z,
                                _selected_object -> first -> matriz_16[12], 
                                _selected_object -> first -> matriz_16[13], 
                                _selected_object -> first -> matriz_16[14],
                                _selected_object -> first -> matriz_16[4],  
                                _selected_object -> first -> matriz_16[5],  
                                _selected_object -> first -> matriz_16[6]);
                    
                    guardarCambios();
                    
                    analisis = TRUE;
                    vuelo =    FALSE; 
  
                }                
            }

            status();
            break;

        //  Sistema de Referencia del Objeto:
        
        case 'l': case 'L':
        
            if (trans_obj){
                if(mundo){
                    mundo   = FALSE; 
                    objeto  = TRUE;             
                }                     
            } else if(trans_cam){                
                if (analisis){
                    analisis   = FALSE;
                    vuelo      = TRUE;
                }
            }

            status(); 
            break;
        
        // Cámara en modo perspectiva
        case 'p': case 'P':
            
            if(trans_cam){
                
                if (pers_paral){
                    pers_paral  = FALSE;
                    
                } else {
                    pers_paral = TRUE;
                }

            }

            status(); 
            break;

        //Cambio de cámara
        case 'c':
            if(trans_cam){
                cam_aux = camara1;
                camara1 = camara1->next;            
                status();            
            }
            
            break;

        //Cámara del objeto
        case 'C':
            if(trans_cam) {
                if(_selected_object){
                    cam_aux = camara1;
                    objectCamera();
                    camara1 = _selected_object->camera;
                    cam_obj = TRUE;        
                    trans_obj = TRUE;
                    trans_cam = FALSE;              
                } else 
                    printf("No hay objeto\n");                 
            } else if(cam_obj){
                cam_obj = FALSE;
                camara1 = cam_aux;
                trans_obj = FALSE;
                trans_cam = TRUE;
            }
            status();
        break;


        case 'n': case 'N':
            if(trans_cam){
                anadirCamara();
            } else {
                printf("Se ha de seleccionar cámaras\n");
            }
            status();
        break;

        //Escoger luz 1, bombilla
        case '1':
            if (luz_esc == 1){
                printf("Has deseleccionado la luz 1, bombilla\n");
                luz_esc = -1;
            } else {
                printf("Has seleccionado la luz 1, bombilla\n");
                luz_esc = 1;                
            }
            status();
        break;

        //Escoger luz 2, sol
        case '2':
            if (luz_esc == 2){
                printf("Has deseleccionado la luz 2, sol\n");
                luz_esc = -1;
            } else {
                printf("Has seleccionado la luz 2, sol\n");
                luz_esc = 2;                
            }
            status();
        break;

        //Escoger luz 3, foco de la cámara
        case '3':
            if (luz_esc == 3){
                printf("Has deseleccionado la luz 3, camara\n");
                luz_esc = -1;
            } else {
                printf("Has seleccionado la luz 3, camara\n");
                luz_esc = 3;                
            }
            status();
        break;

        //Escoger luz 4, foco del objeto
        case '4':
            if (luz_esc == 4){
                printf("Has deseleccionado la luz 4, objeto\n");
                luz_esc = -1;
            } else {
                printf("Has seleccionado la luz 4, objeto\n");
                luz_esc = 4;                
            }
            status();
        break;

        //Ctrl z
        case 26: 
            deshacer(); 
            break;

        case 'f': case 'F':

            /*Ask for file*/
            printf("%s", KG_MSSG_SELECT_FILE);
            scanf("%s", fname);
            /*Allocate memory for the structure and read the file*/
            auxiliar_object = (object3d *) malloc(sizeof (object3d));
            read = read_wavefront(fname, auxiliar_object);
            switch (read) {
            /*Errors in the reading*/
            case 1:
                printf("%s: %s\n", fname, KG_MSSG_FILENOTFOUND);
                break;
            case 2:
                printf("%s: %s\n", fname, KG_MSSG_INVALIDFILE);
                break;
            case 3:
                printf("%s: %s\n", fname, KG_MSSG_EMPTYFILE);
                break;
            /*Read OK*/
            case 0:
                /*Insert the new object in the list*/
                auxiliar_object->next = _first_object;
                _first_object = auxiliar_object;
                _selected_object = _first_object;

                
                glMatrixMode(GL_MODELVIEW);
                glLoadIdentity();
                glGetFloatv(GL_MODELVIEW_MATRIX, coor_matrix -> matriz_16);
                _selected_object -> first = coor_matrix;
                _selected_object -> first -> next = NULL;
                _selected_object -> camera = NULL;

                objectCamera();
                objectLight();

                printf("%s\n",KG_MSSG_FILEREAD);
                break;



            }
            break;

        case 9: /* <TAB> */
            if(cam_obj){
                printf("Ahora mismo estamos en la cámara del objeto, no podemos pues cambiar este\n");
            } else {
                _selected_object = _selected_object->next;
                /*The selection is circular, thus if we move out of the list we go back to the first element*/
                if (_selected_object == 0) _selected_object = _first_object;

            }
            break;

        case 127: /* <SUPR> */
            /*Erasing an object depends on whether it is the first one or not*/
            if (_selected_object == _first_object)
            {
                /*To remove the first object we just set the first as the current's next*/
                _first_object = _first_object->next;
                /*Once updated the pointer to the first object it is save to free the memory*/
               
                free(_selected_object);
                /*Finally, set the selected to the new first one*/
                _selected_object = _first_object;

            } else {
                /*In this case we need to get the previous element to the one we want to erase*/
                auxiliar_object = _first_object;
                while (auxiliar_object->next != _selected_object)
                    auxiliar_object = auxiliar_object->next;
                /*Now we bypass the element to erase*/
                auxiliar_object->next = _selected_object->next;
                /*free the memory*/
               
                free(_selected_object);
                /*and update the selection*/
                _selected_object = auxiliar_object;
            }
            break;

        case '-':
            if (glutGetModifiers() == GLUT_ACTIVE_CTRL || trans_cam){
                    /*Increase the projection plane; compute the new dimensions*/
                    wd=(_ortho_x_max-_ortho_x_min)/KG_STEP_ZOOM;
                    he=(_ortho_y_max-_ortho_y_min)/KG_STEP_ZOOM;
                    /*In order to avoid moving the center of the plane, we get its coordinates*/
                    midx = (_ortho_x_max+_ortho_x_min)/2;
                    midy = (_ortho_y_max+_ortho_y_min)/2;
                    /*The the new limits are set, keeping the center of the plane*/
                    _ortho_x_max = midx + wd/2;
                    _ortho_x_min = midx - wd/2;
                    _ortho_y_max = midy + he/2;
                    _ortho_y_min = midy - he/2;
                } else if (trans_obj) {
                    scalate(DOWNSCALE);
                } else if (trans_luz) {
                    if(luz_esc == 3){
                        status();
                        printf("Decrementado el angulo de apertura\n");
                        cam_angle -= 0.5;
                        glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, cam_angle);
                    } else if(luz_esc == 4){
                        status();
                        printf("Decrementado el angulo de apertura\n");
                        focus_angle -= 0.5;
                        glLightf (GL_LIGHT3, GL_SPOT_CUTOFF, focus_angle);
                    } 
                }
            break;

        case '+':

                if (glutGetModifiers() == GLUT_ACTIVE_CTRL || trans_cam){
                    /*Increase the projection plane; compute the new dimensions*/
                    wd=(_ortho_x_max-_ortho_x_min)*KG_STEP_ZOOM;
                    he=(_ortho_y_max-_ortho_y_min)*KG_STEP_ZOOM;
                    /*In order to avoid moving the center of the plane, we get its coordinates*/
                    midx = (_ortho_x_max+_ortho_x_min)/2;
                    midy = (_ortho_y_max+_ortho_y_min)/2;
                    /*The the new limits are set, keeping the center of the plane*/
                    _ortho_x_max = midx + wd/2;
                    _ortho_x_min = midx - wd/2;
                    _ortho_y_max = midy + he/2;
                    _ortho_y_min = midy - he/2;
                } else if(trans_obj){                    
                    scalate(UPSCALE);
                } else if (trans_luz) {
                    if(luz_esc == 3){
                        status();
                        printf("Incrementado el angulo de apertura\n");
                        cam_angle += 0.5;
                        glLightf (GL_LIGHT2, GL_SPOT_CUTOFF, cam_angle);
                    } else if(luz_esc == 4){
                        status();
                        printf("Incrementado el angulo de apertura\n");
                        focus_angle += 0.5;
                        glLightf (GL_LIGHT3, GL_SPOT_CUTOFF, focus_angle);
                    } 
                }
            break;

        case '?':
            print_help();
            break;

        case 27: /* <ESC> */
            exit(0);
            break;


        default:
            /*In the default case we just print the code of the key. This is usefull to define new cases*/
            printf("%d %c\n", key, key);
        }
    /*In case we have do any modification affecting the displaying of the object, we redraw them*/
    glutPostRedisplay();
}


void specialKeys(int key, int x, int y){

    switch (key) {

        case GLUT_KEY_UP:

            printf("ARROW UP\t");
                        
            if (trans_obj){

                if(trasladar)   translate(UP);
                if(escalar)     scalate(UP);
                if(rotar)       rotate(UP);                  
            }

            if (trans_cam){

                if(trasl_camara)    translate(UP);
                if(rot_camara)      rotate(UP); 
                if(vol_vision)      scalate(UP);                  
            } 

            if (trans_luz){
                if(trans_luz)   translate(UP);
                if(rot_camara)  rotate(UP);
            }
            status();
        break;

        case GLUT_KEY_DOWN:

            printf("ARROW DOWN\t");
            
            if (trans_obj){

                if(trasladar)   translate(DOWN);
                if(escalar)     scalate(DOWN);
                if(rotar)       rotate(DOWN);                  
            }

            if (trans_cam){

                if(trasl_camara)    translate(DOWN);
                if(rot_camara)        rotate(DOWN); 
                if(vol_vision)      scalate(DOWN);  
                                  
            } 

            if (trans_luz){
                if(trans_luz)   translate(DOWN);
                if(rot_camara)  rotate(DOWN);
            }
            status();       
        break;

        case GLUT_KEY_LEFT:

            printf("ARROW LEFT\t");
            
             if (trans_obj){

                if(trasladar)   translate(LEFT);
                if(escalar)     scalate(LEFT);
                if(rotar)       rotate(LEFT);                  
            }

            if (trans_cam){

                if(trasl_camara)    translate(LEFT);
                if(rot_camara)        rotate(LEFT); 
                if(vol_vision)      scalate(LEFT);                    
            } 

            if (trans_luz){
                if(trans_luz)   translate(LEFT);
                if(rot_camara)  rotate(LEFT);
            }
            status();
        break;

        case GLUT_KEY_RIGHT:
            printf("ARROW RIGHT\t");
         
            if (trans_obj){

                if(trasladar)   translate(RIGHT);
                if(escalar)     scalate(RIGHT);
                if(rotar)       rotate(RIGHT);                  
            }

            if (trans_cam){

                if(trasl_camara)    translate(RIGHT);
                if(rot_camara)        rotate(RIGHT); 
                if(vol_vision)      scalate(RIGHT);                    
            } 

            if (trans_luz){
                if(trans_luz)   translate(RIGHT);
                if(rot_camara)  rotate(RIGHT);
            }
            status();
        break;

        case GLUT_KEY_PAGE_UP:
            printf("AVPAG\t"); 
            
             if (trans_obj){

                if(trasladar)   translate(AVPAG);
                if(escalar)     scalate(AVPAG);
                if(rotar)       rotate(AVPAG);                  
            }

            if (trans_cam){

                if(trasl_camara)    translate(AVPAG);
                if(rot_camara)        rotate(AVPAG); 
                if(vol_vision)      scalate(AVPAG);                    
            } 

            if (trans_luz){
                if(trans_luz)   translate(AVPAG);
                if(rot_camara)  rotate(AVPAG);
            }
            status();   
        break;

        case GLUT_KEY_PAGE_DOWN:
            printf("REPAG\t");
            
            if (trans_obj){

                if(trasladar)   translate(REPAG);
                if(escalar)     scalate(REPAG);
                if(rotar)       rotate(REPAG);                  
            }

            if (trans_cam){

                if(trasl_camara)    translate(REPAG);
                if(rot_camara)        rotate(REPAG); 
                if(vol_vision)      scalate(REPAG);                    
            } 

            if (trans_luz){
                if(trans_luz)   translate(REPAG);
                if(rot_camara)  rotate(REPAG);
            }
            status();
        break;

        case GLUT_KEY_F9:
            printf("F9\t");

            if(luz) {
                luz = FALSE;
                for(int i = 0; i < nLuces; i++)
                    luces[i] = FALSE;
                glDisable(GL_LIGHTING);

            } else {
                luz = TRUE;
                glEnable(GL_LIGHTING);
                glEnable(GL_LIGHT0);
                glEnable(GL_LIGHT1);
                glEnable(GL_LIGHT2);
                glEnable(GL_LIGHT3);
                //glEnable(GL_LIGHT4);
                glEnable(GL_DEPTH_TEST);
                for(int i = 0; i < nLuces; i++)
                    luces[i] = TRUE;
            }
             status();
        break;

        case GLUT_KEY_F12:
            printf("F12\t"); status();
            if(luz){
                if(flat_smooth){
                    flat_smooth = FALSE;
                    glShadeModel(GL_FLAT);
                    printf("Ahora se visualiza en modo flat\n");
                } else {
                    flat_smooth = TRUE;
                    glShadeModel(GL_SMOOTH);
                    printf("Ahora se visualiza en modo smooth\n");
                }
            }
        break;

        case GLUT_KEY_F1:
            printf("F1\t"); 
            if(luz){
                if(luces[1]){
                    luces[1] = FALSE;
                    glDisable(GL_LIGHT1);
                    printf("Has apagado la bombilla\n");
                } else {
                    luces[1] = TRUE;
                    glEnable(GL_LIGHT1);
                    printf("Has encendido la bombilla\n");
                }
            }
            status();
        break;

        case GLUT_KEY_F2:
            printf("F2\t");
            if(luz){
                if(luces[0]){
                    luces[0] = FALSE;
                    glDisable(GL_LIGHT0);
                    printf("Has apagado el sol\n");
                } else {
                    luces[0] = TRUE;
                    printf("Has encendido el sol\n");
                    glEnable(GL_LIGHT0);
                }
            }
            status();
        break;

        case GLUT_KEY_F3: 
            printf("F3\t");
            if(luz){
                if(luces[2]){
                    luces[2] = FALSE;
                    glDisable(GL_LIGHT2);
                    printf("Has apagado el foco de la cámara\n");
                } else {
                    luces[2] = TRUE;
                    glEnable(GL_LIGHT2);
                    printf("Has encendido el foco de la cámara\n");
                }
            }
            status();
        break;        

        case GLUT_KEY_F4:
            printf("F4\t");

            if(luz){
                if(luces[3]){
                    luces[3] = FALSE;
                    glDisable(GL_LIGHT3);
                    printf("Has apagado el foco del objeto\n");
                } else {
                    luces[3] = TRUE;
                    glEnable(GL_LIGHT3);
                    printf("Has encendido el foco del objeto\n");
                }
            }
            status();
        break;

    }
    glutPostRedisplay();
}

