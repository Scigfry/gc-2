#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include <GL/gl.h>

/** DEFINITIONS **/

#define KG_WINDOW_TITLE                     "Práctica GPO"
#define KG_WINDOW_WIDTH                     600
#define KG_WINDOW_HEIGHT                    400
#define KG_WINDOW_X                         50
#define KG_WINDOW_Y                         50

#define KG_MSSG_SELECT_FILE                 "Idatz ezazu fitxategiaren path-a: "
#define KG_MSSG_FILENOTFOUND                "Fitxategi hori ez da existitzen!!"
#define KG_MSSG_INVALIDFILE                 "Arazoren bat egon da fitxategiarekin ..."
#define KG_MSSG_EMPTYFILE                   "Fitxategia hutsik dago"
#define KG_MSSG_FILEREAD                    "Fitxategiaren irakurketa buruta"

#define KG_STEP_MOVE                        1.00f   //  5.00f
#define KG_STEP_ROTATE                      5.0f    //  10.0f
#define KG_STEP_ZOOM                        0.75
#define KG_STEP_CAMERA_ANGLE                5.0f

#define KG_ORTHO_X_MIN_INIT                -5
#define KG_ORTHO_X_MAX_INIT                 5
#define KG_ORTHO_Y_MIN_INIT                -5
#define KG_ORTHO_Y_MAX_INIT                 5
#define KG_ORTHO_Z_MIN_INIT                -100
#define KG_ORTHO_Z_MAX_INIT                 10000

#define KG_COL_BACK_R                       0.30f
#define KG_COL_BACK_G                       0.30f
#define KG_COL_BACK_B                       0.30f
#define KG_COL_BACK_A                       1.00f

#define KG_COL_SELECTED_R                   1.00f
#define KG_COL_SELECTED_G                   0.75f
#define KG_COL_SELECTED_B                   0.00f

#define KG_COL_NONSELECTED_R                1.00f
#define KG_COL_NONSELECTED_G                1.00f
#define KG_COL_NONSELECTED_B                1.00f

#define KG_COL_X_AXIS_R                     1.0f
#define KG_COL_X_AXIS_G                     0.0f
#define KG_COL_X_AXIS_B                     0.0f

#define KG_COL_Y_AXIS_R                     0.0f
#define KG_COL_Y_AXIS_G                     1.0f
#define KG_COL_Y_AXIS_B                     0.0f

#define KG_COL_Z_AXIS_R                     0.0f
#define KG_COL_Z_AXIS_G                     0.0f
#define KG_COL_Z_AXIS_B                     1.0f

#define KG_MAX_DOUBLE                       10E25

/*
  New Varibles:
*/

//  Movement:  

#define UP                                  0
#define DOWN                                1
#define LEFT                                2
#define RIGHT                               3
#define AVPAG                               4
#define REPAG                               5
#define UPSCALE                             6
#define DOWNSCALE                           7

#define TRANSLATE_UNIT                      1.0f
#define SCALE_UP_UNIT                       2.0f
#define SCALE_DOWN_UNIT                     0.5f
#define ROTATE_ANGLE                        5.0f

#define TRUE                                1
#define FALSE                               0


/** STRUCTURES **/

/****************************
 * Structure to store the   *
 * coordinates of 3D points *
 ****************************/
typedef struct {
    GLdouble x, y, z;
} point3;

/*****************************
 * Structure to store the    *
 * coordinates of 3D vectors *
 *****************************/
typedef struct {
    GLdouble x, y, z;
} vector3;

/****************************
 * Structure to store the   *
 * colors in RGB mode       *
 ****************************/
typedef struct {
    GLfloat r, g, b;
} color3;

/****************************
 * Structure to store       *
 * objects' vertices         *
 ****************************/
typedef struct {
    point3 coord;                       /* coordinates,x, y, z */
    GLint num_faces;                    /* number of faces that share this vertex */
    vector3 vertNorm;                   // Vector normal del vertice, sumatorio de las normales de las caras de la cual forma parte normalizado
} vertex;

/****************************
 * Structure to store       *
 * objects' faces or        *
 * polygons                 *
 ****************************/
typedef struct {
    GLint num_vertices;                 /* number of vertices in the face */
    GLint *vertex_table;                /* table with the index of each vertex */
    vector3 faceNorm;                   // Vector normal de la cara, se compone de la combinación de vectores de la cara normalizada
} face;




/****************************
 * Structure to store       *
 * values of TRANSFORMATION *
 * matrices                 *
 ****************************/
struct matrix_list
{
    GLfloat matriz_16[16];               /* contiene 16 valores, se dividen haciendo una matriz de 4x4 */
    struct matrix_list *next;           /* apunta a la siguiente matriz (la previa a la transformación) en caso que haya una, de lo contrario, es null */
};

typedef struct matrix_list matrix_list;

/****************************
 * Structure to store a     *
 * list of lights           *
 ****************************/

/****************************
 * Structure to store a     *
 * list of cameras          *
 ****************************/
typedef struct camera_list{

    int id;                     //Identificador de la cámara

    GLfloat left;                 
    GLfloat right;                       
    GLfloat bottom;    
    GLfloat top;                   
    GLfloat near;                      
    GLfloat far;                         

    GLfloat ortho_x_min;        // Parametros usados                 
    GLfloat ortho_x_max;        // para la proyección         
    GLfloat ortho_y_min;        // en modo ortogonal         
    GLfloat ortho_y_max;        //         

    point3 pos;
    matrix_list *matrix;        // Matrix_list de estados        
    struct camera_list *next;   // Siguiente cámara
    struct camera_list *prev;   // Cámara anterior            
                
} camera_list;


/****************************
 * Structure to store a     *
 * pile of 3D objects       *
 ****************************/
struct object3d{
    GLint num_vertices;                 /* number of vertices in the object*/
    vertex *vertex_table;               /* table of vertices */
    GLint num_faces;                    /* number of faces in the object */
    face *face_table;                   /* table of faces */
    point3 min;                         /* coordinates' lower bounds */
    point3 max;                         /* coordinates' bigger bounds */
    struct object3d *next;              /* next element in the pile of objects */
    matrix_list *first;                 /* matriz con las coordenadas del objeto */
    camera_list * camera;                /* Matriz con los valores de la camara*/
    GLfloat *mat_specular;
    GLfloat *mat_ambient;
    GLfloat *mat_diffuse;
    GLfloat *mat_shininess;
};

typedef struct object3d object3d;







#endif // DEFINITIONS_H
